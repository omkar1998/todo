import 'package:flutter/material.dart';
import 'package:todos/Screens/SplashScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Todo's",
        theme: new ThemeData(scaffoldBackgroundColor: const Color(0xffF2F3F8)),
      home: SplashScreen()
    );
  }
}

