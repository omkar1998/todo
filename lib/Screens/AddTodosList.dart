import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:todos/Model/Notes.dart';
import 'package:todos/utils/DatabaseHelper.dart';

class NoteDetail extends StatefulWidget {
  final String appBarTitle;
  final Note note;
  final Note note2;

  NoteDetail(this.note, this.note2, this.appBarTitle);

  @override
  State<StatefulWidget> createState() {
    return NoteDetailState(this.note, this.note2, this.appBarTitle);
  }
}

class NoteDetailState extends State<NoteDetail> {
  var _formkey = GlobalKey<FormState>();
  static var _priorities = ['Not-Completed', 'Completed'];

  DatabaseHelper helper = DatabaseHelper();

  String appBarTitle;
  Note note;
  Note note2;

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  NoteDetailState(this.note, this.note2, this.appBarTitle);

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;

    titleController.text = note.title;
    descriptionController.text = note2.description;

    return WillPopScope(
        onWillPop: () {
          moveToLastScreen();
        },
        child: Scaffold(
            appBar: AppBar(
              title: Text(appBarTitle),
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    // Write some code to control things, when user press back button in AppBar
                    moveToLastScreen();
                  }),
            ),
            body: Form(
              key: _formkey,

                  child: Padding(
                    padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          // First element(Drop Down Button)
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0,left:10.0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Colors.white
                              ),

                              width: double.infinity,
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                    items: _priorities.map((String dropDownStringItem) {
                                      return DropdownMenuItem<String>(
                                        value: dropDownStringItem,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(dropDownStringItem),
                                        ),
                                      );
                                    }).toList(),
                                    style: textStyle,
                                    value: getPriorityAsString(note.priority),
                                    onChanged: (valueSelectedByUser) {
                                      setState(() {
                                        debugPrint(
                                            'User selected $valueSelectedByUser');
                                        updatePriorityAsInt(valueSelectedByUser);
                                      });
                                    }),
                              ),
                            ),
                          ),

                          SizedBox(
                            height: 20.0,
                          ),
                          Stack(alignment: const Alignment(1.0, 1.0), children: <
                              Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                              child: Card(
                                elevation: 0.0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                // height: 100,

                                child: Container(
                                    height: 80,
                                    //  height: 160,
                                    child: Padding(
                                        padding:
                                            EdgeInsets.only(left: 5.0, right: 10.0),
                                        child: Theme(
                                          data: new ThemeData(
                                            primaryColor: Colors.white,
                                            primaryColorDark: Colors.white,
                                          ),
                                          child: TextFormField(
                                              controller: titleController,
                                              validator: (String value) {
                                                if (value.isEmpty) {
                                                  return "Please Enter Title";
                                                }
                                              },
                                              style: textStyle,
                                              onChanged: (value) {
                                                debugPrint(
                                                    'Something changed in Title Text Field');

                                                updateTitle();
                                              },
                                              decoration: InputDecoration(
                                                counterText: "",
                                                errorStyle: TextStyle(
                                                  fontSize: 10,
                                                  color: Colors.red,
                                                ),
                                                hintText: "Title",
                                                hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  decoration: TextDecoration.none,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 18,
                                                ),
                                                filled: true,
                                                fillColor: Colors.white,
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(10.0),
                                                  borderSide: BorderSide(
                                                    color: Colors.white,
                                                    width: 2.0,
                                                  ),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Colors.black12),
                                                  borderRadius:
                                                      const BorderRadius.all(
                                                    const Radius.circular(10.0),
                                                  ),
                                                ),
                                              )),
                                        ))),
                              ),
                            ),
                          ]),
                          SizedBox(
                            height: 20.0,
                          ),
                          Stack(alignment: const Alignment(1.0, 1.0), children: <
                              Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                              child: Card(
                                elevation: 0.0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                // height: 100,

                                child: Container(
                                    height: 150,
                                    //  height: 160,
                                    child: Padding(
                                        padding:
                                            EdgeInsets.only(left: 5.0, right: 10.0),
                                        child: Theme(
                                          data: new ThemeData(
                                            primaryColor: Colors.white,
                                            primaryColorDark: Colors.white,
                                          ),
                                          child: TextFormField(
                                              controller: descriptionController,
                                              validator: (String value) {
                                              },
                                              style: textStyle,
                                              onChanged: (value) {
                                                debugPrint(
                                                    'Something changed in Title Text Field');
                                                updateDescription();
                                              },
                                              decoration: InputDecoration(
                                                counterText: "",
                                                errorStyle: TextStyle(
                                                  fontSize: 10,
                                                  color: Colors.red,
                                                ),
                                                hintText: "Description",
                                                hintStyle: TextStyle(
                                                  color: Colors.black38,
                                                  decoration: TextDecoration.none,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 18,
                                                ),
                                                filled: true,
                                                fillColor: Colors.white,
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(10.0),
                                                  borderSide: BorderSide(
                                                    color: Colors.white,
                                                    width: 2.0,
                                                  ),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Colors.black12),
                                                  borderRadius:
                                                      const BorderRadius.all(
                                                    const Radius.circular(10.0),
                                                  ),
                                                ),
                                              )),
                                        ))),
                              ),
                            ),
                          ]),

                          SizedBox(
                            height: 20.0,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Align(
                                    alignment: FractionalOffset.bottomCenter,
                                    child: Container(
                                        height: 40,
                                        //  width: double.infinity,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              left: 10.0, right: 10.0),
                                          child: Material(
                                            elevation: 5,
                                            color: Color(0xff4A66FB),
                                            borderRadius:
                                            BorderRadius.circular(10.0),
                                            child: MaterialButton(
                                              onPressed: () async {
                                                if (_formkey.currentState
                                                    .validate()) {
                                                  _save();
                                                }
                                              },
                                              minWidth: 200.0,
                                              height: 45.0,
                                              child: Text(
                                                "Save",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 15.0),
                                              ),
                                            ),
                                          ),
                                        ))),
                              ),
                              Expanded(
                                child: Align(
                                    alignment: FractionalOffset.bottomCenter,
                                    child: Container(
                                        height: 40,
                                        //  width: double.infinity,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              left: 10.0, right: 10.0),
                                          child: Material(
                                            elevation: 5,
                                            color: Colors.red,
                                            borderRadius:
                                            BorderRadius.circular(10.0),
                                            child: MaterialButton(
                                              onPressed: () async {
                                                if (_formkey.currentState
                                                    .validate()) {
                                                  _delete();
                                                }
                                              },
                                              minWidth: 200.0,
                                              height: 45.0,
                                              child: Text(
                                                "Delete",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 15.0),
                                              ),
                                            ),
                                          ),
                                        ))),
                              ),
                            ],
                          ),

                        ],
                      ),
                    ),
                  ),

            )));
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  // Convert the String priority in the form of integer before saving it to Database
  void updatePriorityAsInt(String value) {
    switch (value) {
      case 'Not-Completed':
        note.priority = 1;
        //Navigator.push(context, MaterialPageRoute(builder: (context)=> ))
        break;
      case 'Completed':
        note.priority = 2;
        break;
    }
  }

  // Convert int priority to String priority and display it to user in DropDown
  String getPriorityAsString(int value) {
    String priority;
    switch (value) {
      case 1:
        priority = _priorities[0]; // 'High'
        break;
      case 2:
        priority = _priorities[1]; // 'Low'
        break;
    }
    return priority;
  }

 //updating title
  void updateTitle() {
    note.title = titleController.text;
  }

  // updating descp
  void updateDescription() {
    note2.description = descriptionController.text;
  }

  // Save data to database
  void _save() async {
    moveToLastScreen();
    int result;
    if (note.id != null) {
      result = await helper.updateNote(note, note2);
    } else {
      result = await helper.insertNote(note, note2);
      updateDescription();
    }
    if (result != 0) {
      print("Added Successfully");
      _showAlertDialog('Status', 'Note Saved Successfully');
    } else {
      _showAlertDialog('Status', 'Problem Saving Note');
    }
  }

  void _delete() async {
    moveToLastScreen();


    if (note.id == null) {
      _showAlertDialog('Status', 'No Note was deleted');
      return;
    }
    int result = await helper.deleteNote(note.id);
    if (result != 0) {
      _showAlertDialog('Status', 'Note Deleted Successfully');
    } else {
      _showAlertDialog('Status', 'Error Occured while Deleting Note');
    }
  }

  void _showAlertDialog(String title, String message) {
    AlertDialog alertDialog = AlertDialog(
      title: Text(title),
      content: Text(message),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }
}
