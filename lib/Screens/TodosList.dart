import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todos/Model/Notes.dart';
import 'package:todos/utils/DatabaseHelper.dart';
import 'AddTodosList.dart';

class TodosListSccreen extends StatefulWidget {
  @override
  _TodosListSccreenState createState() => _TodosListSccreenState();
}

class _TodosListSccreenState extends State<TodosListSccreen> {

  DatabaseHelper databaseHelper = DatabaseHelper();
  List<Note> noteList;
  int count;

  @override
  Widget build(BuildContext context) {
    if (noteList == null) {
      noteList = List<Note>();
      updateListView();
    }

    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text("${getWelcomeMessages()}, Todo's"),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              navigateToDetail(Note('', 2), Note('', 2), 'Add Note');
            },
            icon: Icon(Icons.add),
          )
          // FlatButton(
          //     onPressed: () => {},
          //     child: Row(
          //       children: <Widget>[
          //         //Icon(Icons.arrow_drop_down_circle,),
          //         Text(
          //           "Total Count = $count",
          //           style: TextStyle(color: Colors.white),
          //         )
          //       ],
          //     )),
        ],
      ),
      body: Column(
        children: <Widget>[
          DefaultTabController(
              length: 2, // length of tabs
              initialIndex: 0,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                      child: Container(
                        height: 70.0,

                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(color: Colors.white)),
                        //height: 100,
                        child: TabBar(
                          isScrollable: false,
                          labelColor: Colors.black,
                          //  indicatorColor: Colors.red,
                          unselectedLabelColor: Colors.black38,

                          indicatorColor: Colors.transparent,
                          tabs: [
                            Tab(
                              child: Container(
                                // width: 80.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    child: Text("Pending(${count})",
                                        // _userModel == null
                                        //     ? ""
                                        //     : "Project(${_userModel.projectsResource.length.toString()})",
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          //    color: Colors.black,
                                          decoration: TextDecoration.none,
                                          fontFamily: 'FuturaPT',
                                          fontWeight: FontWeight.w500,
                                          fontSize: 13,
                                        )),
                                  ),
                                ),
                              ),
                            ),
                            Tab(
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    child: Text("Completed(${count})",
                                        // _userModel == null
                                        //     ? ""
                                        //     : "Project(${_userModel.projectsResource.length.toString()})",
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          //    color: Colors.black,
                                          decoration: TextDecoration.none,
                                          fontFamily: 'FuturaPT',
                                          fontWeight: FontWeight.w500,
                                          fontSize: 13,
                                        )),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    SingleChildScrollView(
                        physics: AlwaysScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        child: Container(

                            height: 450,
                            //height: double.infinity,
                            //height of TabBarView
                            child: TabBarView(children: <Widget>[
                              //onBench(Current)
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5.0, right: 5.0),
                                child: Container(

                                    child: Column(children: <Widget>[
                                  Expanded(
                                      child: ListView.builder(
                                    itemCount: count,
                                    //by removing this Range Error occurs
                                    itemBuilder:
                                        (BuildContext context, int position) {
                                      return Card(
                                        color: Colors.white,
                                        elevation: 2.0,
                                        child: ListTile(
                                          leading: CircleAvatar(
                                            backgroundColor: getPriorityColor(
                                                this
                                                    .noteList[position]
                                                    .priority),
                                            child: getPriorityIcon(this
                                                .noteList[position]
                                                .priority),
                                          ),
                                          title: Text(
                                            this.noteList[position].title,
                                          ),
                                          subtitle: Text(priorityCompletedText(
                                              this
                                                  .noteList[position]
                                                  .priority)),
                                          trailing: GestureDetector(
                                            child: Icon(
                                              Icons.delete,
                                              color: Colors.grey,
                                            ),
                                            onTap: () {
                                              _promptRemoveTodoItem(position);
                                              //Navigator.pop(context);

                                              // _delete(context, noteList[position]);
                                            },
                                          ),
                                          onTap: () {
                                            debugPrint("ListTile Tapped");
                                            navigateToDetail(
                                                this.noteList[position],
                                                this.noteList[position],
                                                'Edit Note');
                                          },
                                        ),
                                      );
                                    },
                                  )

//                                                          _

                                      // style: TextStyle(
                                      //   color: Colors.black54,
                                      //   fontWeight:
                                      //       FontWeight.bold,
                                      //   fontSize: 14.0),
                                      ),
                                ])),
                              ),

                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 5.0, right: 5.0),
                                child: Container(
                                    child: Column(children: <Widget>[
                                  Expanded(
                                      child: ListView.builder(
                                    itemCount: count,
                                    //by removing this Range Error occurs
                                    itemBuilder:
                                        (BuildContext context, int position) {
                                      return Card(
                                        color: Colors.white,
                                        elevation: 2.0,
                                        child: ListTile(
                                          leading: CircleAvatar(
                                            backgroundColor: getPriorityColor(
                                                this
                                                    .noteList[position]
                                                    .priority),
                                            child: getPriorityIcon(this
                                                .noteList[position]
                                                .priority),
                                          ),
                                          title: Text(
                                            this.noteList[position].title,
                                          ),
                                          subtitle: Text(priorityCompletedText(
                                              this
                                                  .noteList[position]
                                                  .priority)),
                                          trailing: GestureDetector(
                                            child: Icon(
                                              Icons.delete,
                                              color: Colors.grey,
                                            ),
                                            onTap: () {
                                              _delete(
                                                  context, noteList[position]);
                                            },
                                          ),
                                          onTap: () {
                                            debugPrint("ListTile Tapped");
                                            navigateToDetail(
                                                this.noteList[position],
                                                this.noteList[position],
                                                'Edit Note');
                                          },
                                        ),
                                      );
                                    },
                                  )),
                                ])),
                              ),
                              //Project
                            ])))
                  ])),
        ],
      ),
    ));
  }

  priorityCompletedText(int priority) {
    Color color = getPriorityColor(priority);
    String msg;

    if (color == Colors.green) {
      msg = "Completed";
    } else if (color == Colors.red) {
      msg = 'Not Completed';
    } else {
      msg = 'L Completed';
    }
    return msg;
  }

  Color getPriorityColor(int priority) {
    switch (priority) {
      case 1:
        return Colors.red;
        break;
      case 2:
        return Colors.green;
        break;

      default:
        return Colors.green;
    }
  }

  Icon getPriorityIcon(int priority) {
    switch (priority) {
      case 1:
        return Icon(
          Icons.cancel,
          color: Colors.white,
        );
        break;
      case 2:
        return Icon(
          Icons.done,
          color: Colors.white,
        );
        break;

      default:
        return Icon(Icons.done_outline);
    }
  }

  void _delete(BuildContext context, Note note) async {
    int result = await databaseHelper.deleteNote(note.id);
    if (result != 0) {
      print("Deleted Successfully");
      updateListView();
    }
  }
  void navigateToDetail(Note note, Note note2, String title) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return NoteDetail(note, note2, title);
    }));

    if (result == true) {
      updateListView();
    }
  }

  void updateListView() {
    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {
      Future<List<Note>> noteListFuture = databaseHelper.getNoteList();
      noteListFuture.then((noteList) {
        setState(() {
          this.noteList = noteList;
          this.count = noteList.length;
        });
      });
    });
  }

  static String getWelcomeMessages() {
    final hour = DateTime.now().hour;
    String msg;
    if (hour < 12) {
      msg = "Good Morning";
    } else if (hour < 18) {
      msg = "Good Afternoon";
    } else {
      msg = "Good Evening";
    }
    return msg;
  }

  _promptRemoveTodoItem(int index) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new AlertDialog(
              title: new Text("Are You Sure ?\nYou won't be able to revert this "),
              actions: <Widget>[
                 new FlatButton(


                      color: Colors.blue,
                      child: new Text('Cancel', style: TextStyle(
                          color: Colors.white
                      ),),
              onPressed: () => Navigator.of(context).pop()),

                new FlatButton(
                  color: Colors.red,
                    child: new Text('Delete', style: TextStyle(
                      color: Colors.white
                    ),),
                    onPressed: () {
                      ;
                      _delete(context, noteList[index]);
                      Navigator.of(context).pop();
                    })
              ]);
        });
  }
}
